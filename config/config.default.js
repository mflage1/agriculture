/* eslint valid-jsdoc: "off" */

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {
    cluster: {
      listen: {
        path: '',
        port: 7001,// 这里就是你要修改的端口号
        hostname: '0.0.0.0', // 0.0.0.0
      }
    }
  };

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1703694218851_5742';

  // add your middleware config here
  config.middleware = [];

  config.mysql = {
    client: {
      host: '124.223.108.7',
      port: "3306",
      user: "Sports",
      password: "eAcMk3bnTXeZpwTz",
      database: "sports"
    },
    app: true,
    agent: false
  }
  config.security = {
    csrf: {
      enable: false,
      ignoreJSON: true
    },
    // domainWhiteList: ['http://www.baidu.com', 'http://localhost:8080'], //配置白名单
  };
  config.cors = {
    origin: '*', 	//允许所有跨域访问
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH',
    credentials: true,	//客户端请求如果需要保存本地凭条(cookie)，则会带有特别的请求字段 withCredentials，服务端需要同样开启这个字段才能响应这些请求
  };
  // 全局配置jwt
  config.jwt = {
    secret: "myproject" //这个是加密秘钥，自行添加
  };
  config.multipart = {
    mode: 'file'
  }

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
    uploadDir: 'app/public/upload'
  };

  return {
    ...config,
    ...userConfig,
  };
};
