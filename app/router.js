/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller,middleware } = app;
  // 为所有的路由加上前缀 api
  router.prefix('/api');
  // 获取菜单
  router.get('/menu',middleware.jwtVerify(app.config.jwt), controller.user.getMenu);
  // 注册
  router.post('/register', controller.user.AddUser);
  // 更新用户信息
  router.post('/updateUser', controller.user.UpdateUser);
  // 登录
  router.post('/login', controller.user.Login);
  // 获取用户信息
  router.get('/getUser', middleware.jwtVerify(app.config.jwt), controller.user.getUser);
  // 获取所有用户
  router.get('/getAllUser', middleware.jwtVerify(app.config.jwt), controller.user.getAllUser);
  // 删除用户
  router.delete('/deleteUser', middleware.jwtVerify(app.config.jwt), controller.user.deleteUser);
  // 上传文件
  router.post('/upload',middleware.jwtVerify(app.config.jwt),controller.upload.uploadFile);
  // 新增公司
  router.post('/addCompany',middleware.jwtVerify(app.config.jwt),controller.company.AddCompany);
  // 获取公司
  router.get('/getCompany',controller.company.getCompany);
  // 更新公司
  router.post('/updateCompany',middleware.jwtVerify(app.config.jwt),controller.company.UpdateCompany);
  // 删除公司
  router.post('/deleteCompany',middleware.jwtVerify(app.config.jwt),controller.company.DeleteCompany);
  // 新增合作公司
  router.post('/addCooperation',middleware.jwtVerify(app.config.jwt),controller.cooperation.AddCooperation);
  // 获取合作公司
  router.get('/getCooperation',middleware.jwtVerify(app.config.jwt),controller.cooperation.getCooperation);
  // 删除合作公司
  router.post('/deleteCooperation',middleware.jwtVerify(app.config.jwt),controller.cooperation.DeleteCooperation);
  // 获取器材
  router.get('/getEquipment',controller.equipment.getEquipment);
  // 新增器材
  router.post('/addEquipment',middleware.jwtVerify(app.config.jwt),controller.equipment.AddEquipment);
  // 删除器材
  router.post('/deleteEquipment',middleware.jwtVerify(app.config.jwt),controller.equipment.DeleteEquipment);
  // 更新器材
  router.post('/updateEquipment',middleware.jwtVerify(app.config.jwt),controller.equipment.UpdateEquipment);
  // 获取器材详情
  router.get('/getEquipmentDetail',controller.equipment.getEquipmentDetails);
  // 新增订单
  router.post('/addOrder',middleware.jwtVerify(app.config.jwt),controller.order.AddOrder);
  // 获取订单
  router.get('/getOrder',middleware.jwtVerify(app.config.jwt),controller.order.getOrder);
  // 删除订单
  router.post('/deleteOrder',middleware.jwtVerify(app.config.jwt),controller.order.DeleteOrder);
  // 获取销售数据
  router.get('/getSalesData',middleware.jwtVerify(app.config.jwt),controller.order.getSalesData);
};
