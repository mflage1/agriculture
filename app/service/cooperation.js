// 引入service
const { Service } = require('egg');

let result = function (data = null, code = 200, msg = "操作成功！", date = Date.now()) {
    return {
        code,
        msg,
        data,
        date
    }
}

class CooperationService extends Service {
    // 新增合作公司
    async addCooperation(body) {
        const { ctx, app } = this;
        try {
            if(ctx.role!=1){
                return result(null, 500, "非管理员不可进行操作！");
            }
            if(body.companyid == ctx.company){
                return result(null, 400, "不能与自己公司建立合作！");
            }
            let res = await app.mysql.get("cooperation", { companyId: body.companyid,uid:ctx.id });
            if (res) {
                return result(null, 400, "此公司已经合作过！");
            } else {
                await app.mysql.insert("cooperation", { companyId: body.companyid,uid:ctx.id })
                return result();
            }
        } catch (error) {
            console.log(error);
            return result(null,500,"error");
        }
    }
    // 获取合作公司
    async getCooperation() {
            const { ctx, app } = this;
            try {
                let res = await app.mysql.select("cooperation",{where: {uid: ctx.id}});
                return result(res);
            }
            catch (error) {
                console.log(error);
                return result(null, 500, "error");
            }
    }
    // 删除合作公司
    async deleteCooperation(body) {
        const { ctx, app } = this;
        try {
            if(ctx.role!=1){
                return result(null, 500, "非管理员不可进行操作！");
            }
            await app.mysql.delete('cooperation', {id:body.id});
            return result();
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "删除失败");
        }
    }
}
module.exports = CooperationService
