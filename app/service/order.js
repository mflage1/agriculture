// 引入service
const { Service } = require('egg');

let result = function (data = null, code = 200, msg = "操作成功！", date = Date.now()) {
    return {
        code,
        msg,
        data,
        date
    }
}

class OrderService extends Service {
    // 新增订单
    async addOrder(body) {
        const { ctx, app } = this;
        try {
            body.uid = ctx.id
            body.companyId = ctx.company
            let eq = JSON.parse(body.goodsDetails);
            if(!eq.company || !eq.company.id){
                return result(null, 400, "此公司已经不存在，无法下单！！", Date.now())
            }
            // 校验是否为合作公司
            let cooperation = await app.mysql.get("cooperation", { uid: ctx.id,companyId:eq.company.id })
            if(eq.company.id != ctx.company){
                if(!cooperation){
                    return result(null, 400, "您不是该公司的合作公司，无法下单！", Date.now())
                }
            }
            await app.mysql.insert("sportorder", body)
            // 更新公司器材数据
            let res = await app.mysql.get("company", { id: eq.company.id })
            let count = Number(res.sportCount) - Number(body.count);
            await app.mysql.update("company", { sportCount: count }, { where: { id: eq.company.id } })
            // 更新器材数据
            let res1 = await app.mysql.get("equipment", { id: eq.id })
            let count1 = Number(res1.count) - Number(body.count);
            let sales = Number(res1.sales) + Number(body.count);
            await app.mysql.update("equipment", { count: count1,sales:sales }, { where: { id: eq.id } })
            return result();
        } catch (error) {
            console.log(error);
            return result(null, 500, "error");
        }
    }
    // 获取订单
    async getOrder() {
        const { ctx, app } = this;
        try {
            let sql = `select * from sportorder`;
            if(ctx.query.scales){
                sql += ` where companyId='${ctx.company}'`;
            }else{
                sql += ` where companyId!=${ctx.company}`;
            }
            let res = await app.mysql.query(sql);
            if(res.length>0){
                for(let i=0;i<res.length;i++){
                    let res1 = await app.mysql.get("user", { id: res[i].uid })
                    res[i].UserName = res1.uname;
                    let res2 = await app.mysql.get("company", { id: res[i].companyId })
                    res[i].companyName = res2.Name;
                }
            }
            return result(res);
        }
        catch (error) {
            console.log(error);
            return result(null, 500, "error");
        }
    }
    // 删除订单
    async deleteOrder(body) {
        const { ctx, app } = this;
        try {
            await app.mysql.delete('sportorder', { id: body.id });
            return result();
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "删除失败");
        }
    }
    // 获取订单详情
    async getOrderDetails() {
        const { ctx, app } = this
        try {
            let { id } = ctx.query
            let res = await app.mysql.get('sportorder', { id: id })
            let company = await app.mysql.get("company", { id: res.companyId });
            if (company) {
                res.company = company
            }
            return result(res)
        }
        catch (error) {
            console.log(error);
            return result(error, 500, '获取失败')
        }
    }
    // 获取销售数据
    async getSalesData() {
        const { ctx, app } = this
        try {
            let res = await app.mysql.select('equipment',{ where:{companyId: ctx.company}, columns: ["Name", "sales"] })
            return result(res)
        }
        catch (error) {
            console.log(error);
            return result(error, 500, '获取失败')
        }
    }
}
module.exports = OrderService
