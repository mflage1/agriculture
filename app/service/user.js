// 引入service
const { Service } = require('egg');

let result = function (data = null, code = 200, msg = "操作成功！", date = Date.now()) {
    return {
        code,
        msg,
        data,
        date
    }
}

class UserService extends Service {
    // 新增用户
    async addUser(body) {
        const { ctx, app } = this;
        try {
            let res = await app.mysql.get("user", { uemail: body.uemail });
            if (res) {
                return result(null, 400, "用户已存在！");
            } else {
                await app.mysql.insert("user", body)
                return result();
            }
        } catch (error) {
            console.log(error);
            return null;
        }
    }
    // 更新用户
    async updateUser(body) {
        const { ctx, app } = this;
        try {
            await app.mysql.update('user', body);
            return result();
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "更新失败");
        }
    }
    // 登录
    async login(body) {
        const { ctx, app } = this;
        try {
            let res = await app.mysql.get("user", { uemail: body.tel, upwd: body.pwd });
            if (res) {
                // 用户存在,生成token
                const token = app.jwt.sign({
                    id: res.id,
                    role: res.urole
                }, app.config.jwt.secret);
                return result(token);
            } else {
                return result(null, 500, "用户不存在!");
            }
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "登录失败");
        }
    }
    // 获取详情
    async getUser() {
        const { ctx, app } = this;
        try {
            let res = await app.mysql.get("user", { id: ctx.id }, { columns: ["uname", "uemail", "id", "urole"] });
            if (res) {
                return result(res);
            } else {
                return result("用户不存在!");
            }
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "获取详情失败！");
        }
    }
    // 获取所有用户
    async getAllUser() {
        const { ctx, app } = this;
        try {
            let sql = `select uname, uemail, id, urole,createTime from user`;
            // 模糊匹配Name
            if (ctx.query.Name) {
                sql += ` where uname like '%${ctx.query.Name}%'`;
            }
            let res = await app.mysql.query(sql);
            return result(res);
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "获取用户失败！");
        }
    }
    // 删除用户
    async deleteUser() {
        const { ctx, app } = this;
        try {
            await app.mysql.delete("user", { id: body.id });
            return result();
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "删除失败！");
        }
    }
    // 获取菜单
    async getMenu() {
        const { ctx } = this;
        let data = [
            {
                index: '1',
                label: '公司模块',
                icon: 'el-icon-office-building',
                url: '/company',
                children: [
                    {
                        index: '1-1',
                        label: '公司列表',
                        icon: '',
                        url: '/company'
                    }
                ]
            },
            {
                index: '2',
                label: '器材展示',
                icon: 'el-icon-bicycle',
                url: '/equipment',
                children: []
            }, {
                index: '3',
                label: '销售数据',
                icon: 'el-icon-pie-chart',
                url: '/sales',
                children: []
            },
            {
                index: '4',
                label: '采购订单管理',
                icon: 'el-icon-tickets',
                url: '/order',
                children: []
            }
        ]
        if (ctx.role == 1) {
            let _data = [
                {
                    index: '5',
                    label: '用户管理',
                    icon: 'el-icon-user-solid',
                    url: '/userManager',
                    children: []
                },
                {
                    index:'6',
                    label:'运动器材管理',
                    icon:'el-icon-s-goods',
                    url:'/equipmentManager',
                    children:[]
                },
                // {
                //     index:'7',
                //     label:'销售数据管理',
                //     icon:'el-icon-pie-chart',
                //     url:'/salesManager',
                //     children:[]
                // },
                {
                    index:'7',
                    label:'销售订单管理',
                    icon:'el-icon-tickets',
                    url:'/orderManager',
                    children:[]
                },
                // {
                //     index:'9',
                //     label:'职员经营数据',
                //     icon:'el-icon-s-data',
                //     url:'/staffManager',
                //     children:[]
                // },
                {
                    index:'8',
                    label:'公司管理',
                    icon:'el-icon-office-building',
                    url:'/companyManager',
                    children:[]
                }
            ]
            return result(data.concat(_data))
        } else {
            return result(data)
        }
    }
}
module.exports = UserService
