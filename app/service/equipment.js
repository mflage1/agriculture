// 引入service
const { Service } = require('egg');

let result = function (data = null, code = 200, msg = "操作成功！", date = Date.now()) {
    return {
        code,
        msg,
        data,
        date
    }
}

class EquipmentService extends Service {
    // 新增器材
    async addEquipment(body) {
        const { ctx, app } = this;
        try {
            let _eqName = await app.mysql.get("equipment", { Name: body.Name, companyId: body.companyId});
            if (_eqName) {
                return result(null, 400, "该公司中该器材已存在！");
            }
            await app.mysql.insert("equipment", body)
            let res = await app.mysql.get("company", { id: body.companyId })
            let count = Number(body.count) + Number(res.sportCount);
            await app.mysql.update("company", { sportCount: count }, { where: { id: body.companyId } })
            return result();

        } catch (error) {
            console.log(error);
            return result(null, 500, "error");
        }
    }
    // 获取器材
    async getEquipment() {
        const { ctx, app } = this;
        try {
            let sql = `select * from equipment`;
            // 模糊匹配Name
            if (ctx.query.Name) {
                sql += ` where Name like '%${ctx.query.Name}%'`;
            }
            let res = await app.mysql.query(sql);
            return result(res);
        }
        catch (error) {
            console.log(error);
            return result(null, 500, "error");
        }
    }
    // 删除器材
    async deleteEquipment(body) {
        const { ctx, app } = this;
        try {
            let _res = await app.mysql.get("equipment", { id: body.id } )
            await app.mysql.delete('equipment', { id: body.id });
            let res = await app.mysql.get("company", { id: body.companyId } )
            let count = Number(res.sportCount) - Number(_res.count);
            await app.mysql.update("company", { sportCount: count }, { where: { id: body.companyId } })
            return result();
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "删除失败");
        }
    }
    // 更新器材
    async updateEquipment(body) {
        const { ctx, app } = this;
        try {
            let _old = JSON.parse(JSON.stringify(body));
            delete body.oldCount;
            await app.mysql.update('equipment', body);
            let res = await app.mysql.get("company", { id: body.companyId })
            let count = Number(res.sportCount) - Number(_old.oldCount) + Number(body.count);
            await app.mysql.update("company", { sportCount: count }, { where: { id: body.companyId } })
            return result();
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "更新失败");
        }
    }
    // 获取器材详情
    async getEquipmentDetails() {
        const {ctx,app}  = this
        try{
            let {id} = ctx.query
            let res = await app.mysql.get('equipment',{id:id})
            let company = await app.mysql.get("company", { id: res.companyId });
            if(company){
                res.company = company
            }
            return result(res)
        }
        catch(error){
            console.log(error);
            return result(error,500,'获取失败')
        }
    }
}
module.exports = EquipmentService
