// 引入service
const { Service } = require('egg');

let result = function (data = null, code = 200, msg = "操作成功！", date = Date.now()) {
    return {
        code,
        msg,
        data,
        date
    }
}

class CompanyService extends Service {
    // 新增公司
    async addCompany(body) {
        const { ctx, app } = this;
        try {
            let res = await app.mysql.get("company", { Name: body.Name });
            if (res) {
                return result(null, 400, "公司已存在！");
            } else {
                await app.mysql.insert("company", body)
                return result();
            }
        } catch (error) {
            console.log(error);
            return result(null, 500, "error");
        }
    }
    // 获取公司
    async getCompany() {
        const { ctx, app } = this;
        try {
            let sql = `select * from company`;
            // 模糊查询Name
            if (ctx.query.Name) {
                sql += ` where name like '%${ctx.query.Name}%'`;
            }
            let res = await app.mysql.query(sql);
            return result(res);
        }
        catch (error) {
            console.log(error);
            return result(null, 500, "error");
        }
    }
    // 更新公司
    async updateCompany(body) {
        const { ctx, app } = this;
        try {
            await app.mysql.update('company', body);
            return result();
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "更新失败");
        }
    }
    // 删除公司数据
    async deleteCompany(body) {
        const { ctx, app } = this;
        try {
            await app.mysql.delete('company', { id: body.id });
            return result();
        }
        catch (error) {
            console.log(error);
            return result(error, 500, "删除失败");
        }
    }
}
module.exports = CompanyService
