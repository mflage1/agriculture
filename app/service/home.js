// 引入service
const { Service } = require('egg');

let result = function ( data = null,code = 200, msg = "操作成功！", date = Date.now()) {
    return {
        code,
        msg,
        data,
        date
    }
}

class HomeService extends Service {
    // 创建一个异步函数来对数据库进行操作
    async getUserInfo() {
        const { ctx, app } = this;
        // sql 语句
        let sql = 'select * from manager';
        try {
            // 通过app.mysql来调用query查询方法（还有get也属于一种查询方法）
            const userInfo = await app.mysql.query(sql);
            return userInfo;
        } catch (error) {
            console.log(error);
            return null;
        }
    }
    async getOneBook(id) {
        const { ctx, app } = this;
        try {
            const res = await app.mysql.get("books", { id: id })
            return result(res);
        } catch (error) {
            console.log(error);
            return null;
        }
    }
    async getAllBooks() {
        const { ctx, app } = this;
        try {
            const res = await app.mysql.select("books")
            return result(res);
        } catch (error) {
            console.log(error);
            return null;
        }
    }
}
module.exports = HomeService
