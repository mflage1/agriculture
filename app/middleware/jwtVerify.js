'use strict';

// 定制白名单
const whiteList = ['/login', '/register'];

module.exports = (options) => {
    return async function (ctx, next) {
        if (!whiteList.some(item => item == ctx.request.url)) {//判断接口路径是否在白名单
            let token = ctx.request.header.authorization//拿到token
            if (token) {//如果token存在
                try {
                    let decoded = ctx.app.jwt.verify(token, ctx.app.config.jwt.secret)//解密token
                    if (decoded && decoded.id) {
                        ctx.id = decoded.id//把id存在ctx上，方便后续操作。
                        ctx.role = decoded.role
                        ctx.company = decoded.company
                        await next()
                    } else {
                        ctx.id = decoded.id//把id存在ctx上，方便后续操作。
                        ctx.role = decoded.role
                        ctx.company = decoded.company
                        await next()
                    }
                } catch (e) {
                    console.log(e, '222');
                }
            } else {
                ctx.body = {
                    code: 403,
                    msg: '无效的token',
                    date: Date.now()
                }
            }
        } else {
            await next()
        }
    }
}