const { Controller } = require('egg');

class HomeController extends Controller {
  async getUser(){
    const { ctx } = this;
    // 异步请求service里面test中的getUserInfo函数
		const resultInfo = await ctx.service.home.getUserInfo();
    ctx.body = resultInfo;
  }
  async getBookId(){
    const {ctx} = this
    const res = await ctx.service.home.getOneBook(ctx.query.id)
    ctx.body = res
  }
}

module.exports = HomeController;
