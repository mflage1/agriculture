const { Controller } = require('egg');

class EquipmentController extends Controller {
  async AddEquipment() {
    const { ctx } = this;
    const resultInfo = await ctx.service.equipment.addEquipment(ctx.request.body);
    ctx.body = resultInfo;
  }
  async getEquipment(){
    const { ctx } = this;
    const resultInfo = await ctx.service.equipment.getEquipment();
    ctx.body = resultInfo;
  }
  async DeleteEquipment() {
    const { ctx } = this;
    const resultInfo = await ctx.service.equipment.deleteEquipment(ctx.request.body);
    ctx.body = resultInfo;
  }
  async UpdateEquipment() {
    const { ctx } = this;
    const resultInfo = await ctx.service.equipment.updateEquipment(ctx.request.body);
    ctx.body = resultInfo;
  }
  async getEquipmentDetails(){
    const { ctx } = this;
    const resultInfo = await ctx.service.equipment.getEquipmentDetails(ctx.request.body);
    ctx.body = resultInfo;
  }
}

module.exports = EquipmentController;
