const { Controller } = require('egg');

class CompanyController extends Controller {
  async AddCompany() {
    const { ctx } = this;
    const resultInfo = await ctx.service.company.addCompany(ctx.request.body);
    ctx.body = resultInfo;
  }
  async getCompany(){
    const { ctx } = this;
    const resultInfo = await ctx.service.company.getCompany();
    ctx.body = resultInfo;
  }
  async DeleteCompany() {
    const { ctx } = this;
    const resultInfo = await ctx.service.company.deleteCompany(ctx.request.body);
    ctx.body = resultInfo;
  }
  async UpdateCompany() {
    const { ctx } = this;
    const resultInfo = await ctx.service.company.updateCompany(ctx.request.body);
    ctx.body = resultInfo;
  }
}

module.exports = CompanyController;
