const { Controller } = require('egg');

class CooperationController extends Controller {
  async AddCooperation() {
    const { ctx } = this;
    const resultInfo = await ctx.service.cooperation.addCooperation(ctx.request.body);
    ctx.body = resultInfo;
  }
  async getCooperation(){
    const { ctx } = this;
    const resultInfo = await ctx.service.cooperation.getCooperation();
    ctx.body = resultInfo;
  }
  async DeleteCooperation() {
    const { ctx } = this;
    const resultInfo = await ctx.service.cooperation.deleteCooperation(ctx.request.body);
    ctx.body = resultInfo;
  }
}

module.exports = CooperationController;
