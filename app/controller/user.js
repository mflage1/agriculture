const { Controller } = require('egg');

class UserController extends Controller {
  async AddUser() {
    const { ctx } = this;
    const resultInfo = await ctx.service.user.addUser(ctx.request.body);
    ctx.body = resultInfo;
  }
  async UpdateUser() {
    const { ctx } = this;
    const resultInfo = await ctx.service.user.updateUser(ctx.request.body);
    ctx.body = resultInfo;
  }
  async Login() {
    const { ctx } = this;
    const resultInfo = await ctx.service.user.login(ctx.request.body);
    ctx.body = resultInfo;
  }
  async getUser() {
    const { ctx } = this
    const resultInfo = await ctx.service.user.getUser();
    ctx.body = resultInfo;
  }
  async getMenu(){
    const { ctx } = this
    const resultInfo = await ctx.service.user.getMenu();
    ctx.body = resultInfo;
  }
  async getAllUser(){
    const { ctx } = this
    const resultInfo = await ctx.service.user.getAllUser();
    ctx.body = resultInfo;
  }
  async deleteUser(){
    const { ctx } = this
    const resultInfo = await ctx.service.user.deleteUser(ctx.request.body);
    ctx.body = resultInfo;
  }
}

module.exports = UserController;
