const { Controller } = require('egg');

class OrderController extends Controller {
  async AddOrder() {
    const { ctx } = this;
    const resultInfo = await ctx.service.order.addOrder(ctx.request.body);
    ctx.body = resultInfo;
  }
  async getOrder(){
    const { ctx } = this;
    const resultInfo = await ctx.service.order.getOrder();
    ctx.body = resultInfo;
  }
  async DeleteOrder() {
    const { ctx } = this;
    const resultInfo = await ctx.service.order.deleteOrder(ctx.request.body);
    ctx.body = resultInfo;
  }
  async getOrderDetails(){
    const { ctx } = this;
    const resultInfo = await ctx.service.order.getOrderDetails(ctx.request.body);
    ctx.body = resultInfo;
  }
  async getSalesData(){
    const { ctx } = this
    const resultInfo = await ctx.service.order.getSalesData(ctx.request.body);
    ctx.body = resultInfo;
    }
}

module.exports = OrderController;
